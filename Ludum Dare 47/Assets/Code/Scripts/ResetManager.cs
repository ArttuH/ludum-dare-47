﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject dataKeeperObject = GameObject.Find("DataKeeper(Clone)");

        if (dataKeeperObject != null)
        {
            Debug.Log("ResetManager found DataKeeper(Clone), destroying it");
            Destroy(dataKeeperObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
