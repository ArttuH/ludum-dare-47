﻿using UnityEngine;

public class PlayerAim : MonoBehaviour
{
    public Camera mainCamera;
    public Transform aimObject;

    private Vector3 aimDirection;

    public GameObject reticle;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        Aim();
    }

    private void Aim()
    {
        Vector3 cursorPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);

        cursorPosition.z = 0;

        aimDirection = (cursorPosition - transform.position).normalized;

        aimObject.LookAt(transform.position + aimDirection);
    }

    public Vector3 GetAimDirection()
    {
        return aimDirection;
    }

    public void SetReticle(bool enabled)
    {
        reticle.SetActive(enabled);
    }
}
