﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PauseScreenScript : MonoBehaviour
{
    public MusicManager musicManager;
    public NavigationManager navigationManager;
    public GameManager gameManager;

    public TextMeshProUGUI soundToggleText;

    public string soundOnToOffText;
    public string soundOffToOnText;

    private void Start()
    {
        if (!musicManager)
        {
            // This is very hacky
            musicManager = GameObject.Find("Ambience(Clone)").GetComponent<MusicManager>();
        }

        if (!navigationManager)
        {
            // This is quite hacky
            navigationManager = GameObject.Find("NavigationManager").GetComponent<NavigationManager>();
        }

        if (!gameManager)
        {
            // This is quite hacky
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        }

        UpdateSoundToggleText();
    }

    public void ContinueGame()
    {
        navigationManager.isOverUI = false;
        gameManager.ContinueGame();
    }

    public void PlayAgain()
    {
        navigationManager.isOverUI = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Debug.Log("Playing again!");
    }

    public void ToggleSound()
    {
        Debug.Log("Toggling sound!");
        musicManager.ToggleSound();

        UpdateSoundToggleText();
    }

    private void UpdateSoundToggleText()
    {
        if (musicManager.soundIsEnabled)
        {
            soundToggleText.text = soundOnToOffText;
        }
        else
        {
            soundToggleText.text = soundOffToOnText;
        }
    }

    public void BackToMainMenu()
    {
        navigationManager.isOverUI = false;
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitGame()
    {
        navigationManager.isOverUI = false;
        Debug.Log("Quitting the game!");
        Application.Quit();
    }
}
