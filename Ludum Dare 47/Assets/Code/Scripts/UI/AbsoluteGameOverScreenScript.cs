﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class AbsoluteGameOverScreenScript : MonoBehaviour
{
    public MusicManager musicManager;
    public NavigationManager navigationManager;

    public TextMeshProUGUI soundToggleText;

    public string soundOnToOffText;
    public string soundOffToOnText;

    public GameManager gameManager;

    private void Start()
    {
        if (!musicManager)
        {
            // This is very hacky
            musicManager = GameObject.Find("Ambience(Clone)").GetComponent<MusicManager>();
        }

        if (!navigationManager)
        {
            // This is quite hacky
            navigationManager = GameObject.Find("NavigationManager").GetComponent<NavigationManager>();
        }

        UpdateSoundToggleText();
    }

    public void PlayAgain()
    {
        navigationManager.isOverUI = false;

        gameManager.PurgeDataKeeper();

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Debug.Log("Playing again!");
    }

    public void ToggleSound()
    {
        Debug.Log("Toggling sound!");
        musicManager.ToggleSound();

        UpdateSoundToggleText();
    }

    private void UpdateSoundToggleText()
    {
        if (musicManager.soundIsEnabled)
        {
            soundToggleText.text = soundOnToOffText;
        }
        else
        {
            soundToggleText.text = soundOffToOnText;
        }
    }

    public void BackToMainMenu()
    {
        navigationManager.isOverUI = false;
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitGame()
    {
        navigationManager.isOverUI = false;
        Debug.Log("Quitting the game!");
        Application.Quit();
    }
}
