﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public NavigationManager navigationManager;

    private void Start()
    {
        if (!navigationManager)
        {
            navigationManager = GameObject.Find("NavigationManager").GetComponent<NavigationManager>();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Hovering over " + name);
        navigationManager.EnterOverUI();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("No longer hovering over " + name);
        navigationManager.ExitOverUI();
    }
}
