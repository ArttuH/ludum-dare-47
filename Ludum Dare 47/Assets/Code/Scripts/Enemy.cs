﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Enemy : MonoBehaviour
{
    AIDestinationSetter destinationSetter;

    public EnemyManager enemyManager;

    private void Awake()
    {
        destinationSetter = GetComponent<AIDestinationSetter>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Die()
    {
        enemyManager.EnemyDied(this);
        Destroy(gameObject);
    }

    public void SetTarget(Transform target)
    {
        destinationSetter.target = target;
    }

    public Transform GetTarget()
    {
        return destinationSetter.target;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlatformerCharacter2D>().Die();
        }
        else if (collision.gameObject.CompareTag("PlayerClone"))
        {
            collision.gameObject.GetComponent<PlayerClone>().Die();
        }
    }
}
