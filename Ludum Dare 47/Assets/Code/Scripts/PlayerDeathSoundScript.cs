﻿using UnityEngine;

public class PlayerDeathSoundScript : MonoBehaviour
{
    public AudioClip deathClip;

    private void Start()
    {
        GetComponent<AudioSource>().PlayOneShot(deathClip);
        Destroy(gameObject, 2.5f);
    }
}
