﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataKeeper : MonoBehaviour
{
    public List<PlayerRecording> playerRecordings;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);

        if (playerRecordings == null)
        {
            playerRecordings = new List<PlayerRecording>();
        }
    }
}
