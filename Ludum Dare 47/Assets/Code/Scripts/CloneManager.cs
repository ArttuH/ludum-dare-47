﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneManager : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject clonePrefab;

    public List<PlayerRecording> playerRecordings;

    public List<Vector3> spawnPositions;

    public ProjectilePool projectilePool;
    public GameManager gameManager;

    private List<Transform> playerTransforms; // Includes clones

    private void Awake()
    {
        //DontDestroyOnLoad(gameObject);

        FindSpawnPositions();
    }

    public void CustomStart(GameManager manager, ProjectilePool pool)
    {
        gameManager = manager;
        projectilePool = pool;

        playerTransforms = new List<Transform>();
        SpawnClones();
        SpawnPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FindSpawnPositions()
    {
        Transform spawnPositionsParent = GameObject.Find("SpawnPositions").transform;

        for (int i = 0; i < spawnPositionsParent.childCount; ++i)
        {
            spawnPositions.Add(spawnPositionsParent.GetChild(i).transform.position);
        }
    }

    void SpawnClones()
    {
        if (playerRecordings != null)
        {
            for (int i = 0; i < playerRecordings.Count; ++i)
            {
                GameObject clone = Instantiate(
                    clonePrefab, spawnPositions[i], Quaternion.identity);

                PlayerClone playerClone = clone.GetComponent<PlayerClone>();

                playerClone.playerRecording = new PlayerRecording();
                playerClone.playerRecording.Import(playerRecordings[i]);
                //cloneRecording.Import(playerRecordings[i]);
                playerClone.projectilePool = projectilePool;
                playerClone.cloneManager = this;

                playerTransforms.Add(clone.transform);
            }
        }
    }

    void SpawnPlayer()
    {
        int cloneCount = 0;

        if (playerRecordings != null)
        {
            cloneCount = playerRecordings.Count;
        }

        GameObject playerObject = Instantiate(playerPrefab, spawnPositions[cloneCount], Quaternion.identity);

        PlatformerCharacter2D player = playerObject.GetComponent<PlatformerCharacter2D>();

        player.cloneManager = this;

        playerTransforms.Add(playerObject.transform);
    }

    public List<Transform> GetPlayerTransforms()
    {
        return playerTransforms;
    }

    public void PlayerCloneDied(Transform cloneTransform)
    {
        playerTransforms.Remove(cloneTransform);
        gameManager.enemyManager.PlayerCloneDied(cloneTransform);
    }
}
