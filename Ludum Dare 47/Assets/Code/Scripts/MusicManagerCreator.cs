﻿using UnityEngine;

public class MusicManagerCreator : MonoBehaviour
{
    public GameObject musicManagerObject;

    private void Awake()
    {
        if (!GameObject.Find("Ambience"))
        {
            Instantiate(musicManagerObject);
        }
    }
}
