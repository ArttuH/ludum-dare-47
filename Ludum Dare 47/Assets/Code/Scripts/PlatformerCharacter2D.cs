﻿using UnityEngine;

public class PlatformerCharacter2D : MonoBehaviour
{
    public GameManager gameManager;
    public CloneManager cloneManager;

    public GameObject projectilePrefab;

    public GameObject playerDeathSoundPrefab;

    private float   m_horizontalMovement;
    private bool    m_jump;
    private bool    m_shoot;

    [SerializeField] private float  m_maxSpeed;
    [SerializeField] private float  m_jumpForce;
    [SerializeField] private float  m_shootRate;

    [SerializeField] private float m_projectileOriginDistance;

    Rigidbody2D m_rigidBody2D;

    ProjectilePool projectilePool;

    PlayerAim m_playerAim;
    PlayerRecording m_playerRecording;

    private float m_shootTimer;

    private float m_elapsedTime;

    private bool m_canJump;

    private void Awake()
    {
        m_rigidBody2D       = GetComponent<Rigidbody2D>();
        m_playerAim         = GetComponent<PlayerAim>();

        GameObject gameManagerObject = GameObject.Find("GameManager");

        projectilePool = gameManagerObject.GetComponent<ProjectilePool>();

        gameManager = gameManagerObject.GetComponent<GameManager>();

        m_shootTimer = 0f;
    }

    // Start is called before the first frame update
    void Start()
    {
        m_playerRecording = new PlayerRecording();
        m_elapsedTime = 0f;
        m_canJump = true;
    }

    // Update is called once per frame
    void Update()
    {
        ReadPlayerInput();
    }

    private void FixedUpdate()
    {
        Move();
        Record();

        m_elapsedTime += Time.fixedDeltaTime;
    }

    private void ReadPlayerInput()
    {
        m_horizontalMovement = Input.GetAxis("Horizontal");

        if (m_canJump && !m_jump)
        {
            if (Input.GetButtonDown("Jump") || Input.GetAxisRaw("Vertical") > 0)
            {
                m_jump = true;
            }
        }

        if (m_shootTimer > 0)
        {
            m_shootTimer -= Time.deltaTime;

            if (m_shootTimer <= 0)
            {
                m_playerAim.SetReticle(true);
            }
        }
        else
        {
            if (!m_shoot)
            {
                m_shoot = Input.GetButton("Fire1");
            }
        }
    }

    private void Move()
    {
        m_rigidBody2D.velocity = new Vector2(
            m_horizontalMovement * m_maxSpeed,
            m_rigidBody2D.velocity.y);

        if (m_jump)
        {
            m_rigidBody2D.AddForce(new Vector2(0f, m_jumpForce));

            m_jump = false;
            m_canJump = false;
        }

        if (m_shoot)
        {
            Shoot();

            m_shoot = false;
        }
    }

    private void Shoot()
    {
        Debug.Log("Shooting");

        m_playerAim.SetReticle(false);

        m_shootTimer = 1 / m_shootRate;

        Vector3 aimDirection = m_playerAim.GetAimDirection();

        GameObject projectile = projectilePool.SpawnProjectile(
            transform.position + aimDirection * m_projectileOriginDistance);

        Projectile projectileScript = projectile.GetComponent<Projectile>();

        projectileScript.SetMoveDirection(aimDirection);

        m_playerRecording.shootTimes.Add(m_elapsedTime);
    }

    public void Die()
    {
        Debug.Log("Player died!");

        //cloneManager.playerRecordings.Add(m_playerRecording);

        Instantiate(playerDeathSoundPrefab, transform.position, Quaternion.identity);

        gameManager.GameOver();
    }

    private void Record()
    {
        m_playerRecording.positions.Add(transform.position);
        m_playerRecording.aimRotations.Add(m_playerAim.aimObject.rotation);
    }

    public PlayerRecording GetPlayerRecording()
    {
        return m_playerRecording;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("Player collided with " + collision.collider.name);
        m_canJump = true;
    }
}
