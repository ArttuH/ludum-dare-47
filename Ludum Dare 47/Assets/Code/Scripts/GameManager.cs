﻿using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class GameManager : MonoBehaviour
{
    public GameObject dataKeeperPrefab;
    public GameObject dataKeeperObject;
    public DataKeeper dataKeeperScript;

    public ScoreManager scoreManager;
    public CloneManager cloneManager;
    public EnemyManager enemyManager;

    public GameObject canvas;

    public GameObject playerObject;
    public GameObject ballSpawner;
    
    public GameObject gameOverScreen;
    public GameObject pauseScreen;
    public GameObject victoryScreen;
    public GameObject absoluteGameOverScreen;

    public bool isPaused;

    public CinemachineVirtualCamera virtualCamera;

    bool gameIsActive;

    private void Awake()
    {
        gameIsActive = true;

        isPaused = false;

        cloneManager = GetComponent<CloneManager>();
        enemyManager = GetComponent<EnemyManager>(); 
    }

    private void Start()
    {
        Debug.Log("GameManager Start()");

        canvas.SetActive(true);

        dataKeeperObject = GameObject.Find("DataKeeper(Clone)");

        if (dataKeeperObject != null)
        {
            Debug.Log("Found DataKeeper(Clone)");
            dataKeeperScript = dataKeeperObject.GetComponent<DataKeeper>();
            cloneManager.playerRecordings = dataKeeperScript.playerRecordings;
        }
        else
        {
            Debug.Log("Did not find DataKeeper(Clone)");
        }
        
        cloneManager.CustomStart(this, GetComponent<ProjectilePool>());
        enemyManager.CustomStart(this);

        playerObject = GameObject.Find("PlayerCharacter(Clone)");

        virtualCamera.Follow = playerObject.transform;
    }

    private void Update()
    {
        KeyListener();
    }

    private void KeyListener()
    {
        if (gameIsActive)
        {
            if (Input.GetKeyDown(KeyCode.Escape) ||
                        Input.GetKeyDown(KeyCode.LeftControl) ||
                        Input.GetKeyDown(KeyCode.RightControl))
            {
                if (isPaused)
                {
                    ContinueGame();
                }
                else
                {
                    PauseGame();
                }
            }
        }
    }

    public void Victory()
    {
        DeactivateGame();
        Debug.Log("You won the game!");

        victoryScreen.SetActive(true);
    }

    public void GameOver()
    {
        HandleDataKeeper();

        DeactivateGame();

        if (dataKeeperScript.playerRecordings.Count < 10)
        {
            gameOverScreen.SetActive(true);

            GameOverScreenScript gameOverScreenScript =
                gameOverScreen.GetComponent<GameOverScreenScript>();

            gameOverScreenScript.UpdateTexts(
                enemyManager.remainingEnemyCount,
                10 - dataKeeperScript.playerRecordings.Count);
        }
        else
        {
            absoluteGameOverScreen.SetActive(true);
        }

        Debug.Log("You lost the game!");
    }

    private void DeactivateGame()
    {
        gameIsActive = false;

        playerObject.SetActive(false);

        scoreManager.Pause();
    }

    private void PauseGame()
    {
        Time.timeScale = 0;
        isPaused = true;
        pauseScreen.SetActive(true);
    }

    public void ContinueGame()
    {
        Time.timeScale = 1;
        isPaused = false;
        pauseScreen.SetActive(false);
    }

    private void HandleDataKeeper()
    {
        if (dataKeeperObject == null)
        {
            dataKeeperObject = Instantiate(dataKeeperPrefab);
            dataKeeperScript = dataKeeperObject.GetComponent<DataKeeper>();
            dataKeeperScript.playerRecordings = new List<PlayerRecording>();
        }
        
        dataKeeperScript.playerRecordings.Add(
            playerObject.GetComponent<PlatformerCharacter2D>().GetPlayerRecording());
    }

    public void PurgeDataKeeper()
    {
        if (dataKeeperObject == null)
        {
            dataKeeperObject = GameObject.Find("DataKeeper(Clone)");
        }

        if (dataKeeperObject != null)
        {
            Destroy(dataKeeperObject);
        }
    }
}
