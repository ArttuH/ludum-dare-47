﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClone : MonoBehaviour
{
    public PlayerRecording playerRecording;
    public Transform aimTransform;

    public ProjectilePool projectilePool;

    public GameObject playerDeathSoundPrefab;

    public CloneManager cloneManager;

    private int playbackIndex;
    private int shootIndex;

    [SerializeField] private float projectileOriginDistance;

    private float elapsedTime;

    private void Awake()
    {
        //playerRecording = new PlayerRecording();
        aimTransform = transform.Find("AimObject").transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        playbackIndex = 0;
        shootIndex = 0;
        elapsedTime = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (playbackIndex < playerRecording.positions.Count)
        {
            transform.position = playerRecording.positions[playbackIndex];
            aimTransform.rotation = playerRecording.aimRotations[playbackIndex];
        }

        ++playbackIndex;

        if (shootIndex < playerRecording.shootTimes.Count)
        {
            if (playerRecording.shootTimes[shootIndex] <= elapsedTime)
            {
                Shoot();
                ++shootIndex;
            }
        }

        elapsedTime += Time.fixedDeltaTime;
    }

    private void Shoot()
    {
        Vector3 aimDirection = aimTransform.forward;

        GameObject projectile = projectilePool.SpawnProjectile(
            transform.position + aimDirection * projectileOriginDistance);

        Projectile projectileScript = projectile.GetComponent<Projectile>();

        projectileScript.SetMoveDirection(aimDirection);
    }

    public void Die()
    {
        Debug.Log("Clone " + name + " died!");

        Instantiate(playerDeathSoundPrefab, transform.position, Quaternion.identity);

        cloneManager.PlayerCloneDied(transform);

        gameObject.SetActive(false);
    }
}
