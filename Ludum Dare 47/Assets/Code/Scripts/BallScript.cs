﻿using UnityEngine;

public class BallScript : PoolableObject
{
    public BallSpawnerScript ballSpawnerScript;

    public GameObject spawnParticleSystemPrefab;

    private GameObject spawnParticleSystem;

    public AudioSource spawnAudioSource;
    public AudioSource impactAudioSource;
    public AudioSource idleAudioSource;

    public float speed;
    public float speedIncreaseFactor;

    public float startSpeed;
    public float maxSpeed;

    public float normalDeviancyLimit;

    public int hitCounter;
    public int maxHits;

    public float spawnDelay;

    //private Color[] debugColors = { Color.red, Color.cyan };
    //private int debugColorIndex = 0;

    private bool isActive;

    private void Awake()
    {
        isActive = false;

        spawnParticleSystem = Instantiate(spawnParticleSystemPrefab);
        
        spawnParticleSystem.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            Move();
        }
    }

    public void Appear(Vector3 newPosition)
    {
        isActive = true;
        transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        GetComponent<ParticleSystem>().Play();

        ballSpawnerScript.IncreaseActiveBallCount();

        speed = 0f;

        hitCounter = maxHits;

        transform.position = newPosition;

        transform.LookAt(new Vector3(0, 0, 0));

        spawnParticleSystem.transform.position = transform.position;
        spawnParticleSystem.SetActive(true);
        spawnParticleSystem.GetComponent<ParticleSystem>().Play();

        spawnAudioSource.Play();
        idleAudioSource.Play();

        Invoke("ResetSpeed", spawnDelay);
    }

    private void Disappear()
    {
        isActive = false;
        transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
        GetComponent<ParticleSystem>().Stop();

        ballSpawnerScript.DecreaseActiveBallCount();

        spawnParticleSystem.SetActive(false);
        idleAudioSource.Stop();

        //Debug.Log(name + " will now disappear!");

        Invoke("ReturnToPool", impactAudioSource.clip.length);

        //ReturnToPool();
    }

    public void ChangeDirection(Vector3 newDirection)
    {
        transform.LookAt(transform.position + newDirection);
    }

    public void IncreaseSpeed()
    {
        if (speed < maxSpeed)
        {
            speed *= speedIncreaseFactor;

            if (speed > maxSpeed)
            {
                speed = maxSpeed;
            }
        }
    }

    private void Move()
    {
        float movementDistance = speed * Time.deltaTime;

        transform.position = MoveCollisionCheck(movementDistance);

        //Debug.DrawRay(
        //    transform.position + transform.forward * 0.25f,
        //    transform.forward * movementDistance,
        //    Color.green,
        //    Time.deltaTime
        //    );

        //transform.position = transform.position + transform.forward * movementDistance;
        //transform.Translate(transform.forward * movementDistance, Space.World);
    }

    private Vector3 MoveCollisionCheck(float movementDistance)
    {
        RaycastHit raycastHit;

        bool collisionDetected = Physics.Raycast(transform.position, transform.forward, out raycastHit, movementDistance);

        Vector3 newPosition;

        if (collisionDetected)
        {
            //Debug.Log("Raycast hit " + raycastHit.collider.name);

            impactAudioSource.Play();

            CollisionInteraction(raycastHit);

            float remainingDistance = movementDistance - (raycastHit.point - transform.position).magnitude;

            Vector3 bounceNormal;
            
            // Alter the normal direction of the bounce
            if (raycastHit.collider.CompareTag("Edges"))
            {
                bounceNormal = raycastHit.normal;
            }
            else
            {
                bounceNormal = AlterNormal(raycastHit.normal);
            }

            newPosition = raycastHit.point + bounceNormal * remainingDistance;

            ChangeDirection(bounceNormal);
        }
        else
        {
            newPosition = transform.position + transform.forward * movementDistance;
        }

        //Debug.DrawRay(
        //    transform.position + transform.forward * 0.25f,
        //    transform.forward * movementDistance,
        //    debugColors[debugColorIndex],
        //    1.0f
        //    //Time.deltaTime
        //    );
        //
        //++debugColorIndex;
        //if (debugColorIndex >= debugColors.Length)
        //{
        //    debugColorIndex = 0;
        //}

        return newPosition;

    }

    private void CollisionInteraction(RaycastHit collision)
    {
        if (collision.transform.CompareTag("ShellSection"))
        {
            //ShellSectionScript shellSectionScript = collision.transform.GetComponent<ShellSectionScript>();

            //shellSectionScript.Hit();

            --hitCounter;

            if (hitCounter <= 0)
            {
                Disappear();
            }
        }
        else if (collision.transform.CompareTag("Player"))
        {
            hitCounter = maxHits;

            IncreaseSpeed();
        }
        else if (collision.transform.CompareTag("Core"))
        {
            //CoreScript coreScript = collision.transform.GetComponent<CoreScript>();

            //coreScript.Hit();
        }
    }

    private Vector3 AlterNormal(Vector3 oldNormal)
    {
        float deviancyAngleDegs = Random.Range(-normalDeviancyLimit, normalDeviancyLimit);

        float deviancyAngleRads = deviancyAngleDegs / 180.0f * Mathf.PI;

        float newX = Mathf.Cos(deviancyAngleRads) * oldNormal.x - Mathf.Sin(deviancyAngleRads) * oldNormal.z;
        float newZ = Mathf.Sin(deviancyAngleRads) * oldNormal.x + Mathf.Cos(deviancyAngleRads) * oldNormal.z;

        Vector3 newNormal = new Vector3(newX, oldNormal.y, newZ);

        return newNormal;
    }

    private void ResetSpeed()
    {
        speed = startSpeed;
    }
}
