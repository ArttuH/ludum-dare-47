﻿using UnityEngine;

public class Projectile : PoolableObject
{
    [SerializeField] private float m_moveSpeed;
    private Vector3 m_moveDirection;

    public AudioClip shootClip;
    public AudioClip impactClip;

    public GameObject impactParticleEffect;
    public GameObject enemyDeathParticleEffect;

    private AudioSource audioSource;

    MeshRenderer meshRenderer;

    bool hasCollided;

    // Start is called before the first frame update
    void Start()
    {
        hasCollided = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (!hasCollided)
        {
            CollisionCheckRaycast();
            Move();
        }  
    }

    public void SetMoveDirection(Vector3 moveDirection)
    {
        m_moveDirection = moveDirection;
    }

    private void Move()
    {
        transform.Translate(m_moveDirection * m_moveSpeed);
    }

    private void CollisionCheckRaycast()
    {
        RaycastHit2D hit = Physics2D.Raycast(
            transform.position,
            m_moveDirection,
            m_moveSpeed);

        if (hit.collider != null)
        {
            Debug.Log("Projectile hit " + hit.collider.name);

            if (!hit.collider.gameObject.CompareTag("Player"))
            {
                hasCollided = true;

                GameObject particleEffect = impactParticleEffect;

                if (hit.collider.gameObject.CompareTag("Enemy"))
                {
                    particleEffect = enemyDeathParticleEffect;

                    Enemy enemy = hit.collider.gameObject.GetComponent<Enemy>();

                    enemy.Die();
                }

                // Wasteful jam code
                Destroy(Instantiate(
                    particleEffect,
                    new Vector3(hit.point.x, hit.point.y, 0f),
                    Quaternion.identity), 5f);

                meshRenderer.enabled = false;

                audioSource.PlayOneShot(impactClip);
                
                Invoke("ReturnToPool", 1f);
            }
        }
    }

    public void ResetProjectile()
    {
        hasCollided = false;

        meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.enabled = true;

        audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(shootClip);
    }
}
