﻿using UnityEngine;

public class ProjectilePool : MonoBehaviour
{
    public GameObject projectilePrefab;
    public ObjectPool projectilePool;
    public int projectilePoolInitialSize;

    private void Awake()
    {
        projectilePool.CreatePool(projectilePrefab, projectilePoolInitialSize);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject SpawnProjectile(Vector3 position)
    {
        GameObject projectile = projectilePool.GetPooledObject();

        projectile.transform.position = position;

        projectile.GetComponent<Projectile>().ResetProjectile();

        return projectile;
    }
}
