﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRecording
{
    public List<Vector3> positions;
    public List<Quaternion> aimRotations;
    public List<float> shootTimes;

    public PlayerRecording()
    {
        positions = new List<Vector3>();
        aimRotations = new List<Quaternion>();
        shootTimes = new List<float>();
    }

    public void Import(PlayerRecording other)
    {
        positions = other.positions;
        aimRotations = other.aimRotations;
        shootTimes = other.shootTimes;
    }
}
