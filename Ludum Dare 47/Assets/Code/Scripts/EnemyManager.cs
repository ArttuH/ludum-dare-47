﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyManager : MonoBehaviour
{
    public Transform enemyParent;

    public List<Enemy> enemies;

    public bool isActive;

    public TMPro.TextMeshProUGUI enemyCountText;

    public int remainingEnemyCount;

    public GameManager gameManager;

    private void Awake()
    {
        FindEnemies();
    }

    public void CustomStart(GameManager manager)
    {
        gameManager = manager;

        if (isActive)
        {
            //Transform playerTransform = GameObject.Find("PlayerCharacter(Clone)").transform;
            //SetAllEnemiesTarget(playerTransform);
            SetAllEnemiesTarget();
        }

        SetAllEnemiesAudioPitch();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FindEnemies()
    {
        enemyParent = GameObject.Find("Enemies").transform;

        for (int i = 0; i < enemyParent.childCount; ++i)
        {
            enemies.Add(enemyParent.GetChild(i).GetComponent<Enemy>());
        }

        SetAllEnemiesManager();

        remainingEnemyCount = enemyParent.childCount;

        UpdateEnemyCountText();
    }

    void SetAllEnemiesTarget()
    {
        List<Transform> playerTransforms = gameManager.cloneManager.GetPlayerTransforms();

        foreach (Enemy enemy in enemies)
        {
            SetNearestTarget(enemy, playerTransforms);
        }
    }

    void SetAllEnemiesTarget(Transform target)
    {
        foreach (Enemy enemy in enemies)
        {
            enemy.SetTarget(target);
        }
    }

    void SetAllEnemiesManager()
    {
        foreach (Enemy enemy in enemies)
        {
            enemy.enemyManager = this;
        }
    }

    void SetAllEnemiesAudioPitch()
    {
        foreach (Enemy enemy in enemies)
        {
            enemy.GetComponent<AudioSource>().pitch = Random.Range(0.8f, 1.2f);
        }
    }

    private void UpdateEnemyCountText()
    {
        enemyCountText.text = remainingEnemyCount.ToString();
    }

    public void PlayerCloneDied(Transform cloneTransform)
    {
        List<Transform> playerTransforms = gameManager.cloneManager.GetPlayerTransforms();

        foreach (Enemy enemy in enemies)
        {
            if (enemy.GetTarget() == cloneTransform)
            {
                SetNearestTarget(enemy, playerTransforms);
            }
        }
    }

    public void EnemyDied(Enemy enemy)
    {
        enemies.Remove(enemy);

        remainingEnemyCount -= 1;
        UpdateEnemyCountText();

        if (remainingEnemyCount == 0)
        {
            gameManager.Victory();
        }
    }

    private void SetNearestTarget(Enemy enemy, List<Transform> players)
    {
        Transform nearestTarget = players[0];

        float nearestDistance = (nearestTarget.position - enemy.transform.position).magnitude;

        if (players.Count > 1)
        {
            foreach (Transform target in players)
            {
                float newDistance = (target.position - enemy.transform.position).magnitude;

                if (newDistance < nearestDistance)
                {
                    nearestTarget = target;
                }
            }
        }

        enemy.SetTarget(nearestTarget);
    }
}
