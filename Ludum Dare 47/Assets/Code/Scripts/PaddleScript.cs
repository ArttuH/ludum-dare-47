﻿using UnityEngine;

public class PaddleScript : MonoBehaviour
{
    public float rotationSpeed;

    private bool isActive;

    private void Start()
    {
        isActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            KeyListener();
        }
    }

    public void Deactivate()
    {
        isActive = false;
    }

    private void KeyListener()
    {
        Rotate(Input.GetAxis("Horizontal"));
    }

    private void Rotate(float direction)
    {
        transform.Rotate(Vector3.up, rotationSpeed * direction * Time.deltaTime);
    }
}
